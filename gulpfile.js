
var gulp            = require('gulp');
var sass            = require('gulp-sass');
var watch           = require('gulp-watch');
var browserSync     = require('browser-sync');
var reload          = browserSync.reload;
var concat          = require('gulp-concat');
var jshint          = require('gulp-jshint');
var del             = require('del');
var uglify          = require('gulp-uglify');
var imagemin        = require('gulp-imagemin');
var sourcemaps      = require('gulp-sourcemaps');
var autoprefixer    = require('gulp-autoprefixer');
var rubysass        = require('gulp-ruby-sass');
var gcmq            = require('gulp-group-css-media-queries');
var uglifycss       = require('gulp-uglifycss');
var purify          = require('purify-css');




// source and distribution folder
var source = 'src/';
var dest = 'dist/assets/';

// Bootstrap scss source
var bootstrapSass = {
        in: './node_modules/bootstrap-sass/'
    };

// fonts
var fonts = {
        in: [source + 'fonts/*.*', bootstrapSass.in + 'assets/fonts/**/*'],
        out: dest + 'fonts/'
    };

// css source file: .scss files
var scss = {
    in: source + 'scss/main.scss',
    out: dest + 'css/',
    watch: source + 'scss/**/*',

    sassOpts: {
        outputStyle: 'nested',
        precison: 3,
        errLogToConsole: true,
        includePaths: [bootstrapSass.in + 'assets/stylesheets']
    }
};

// copy bootstrap required fonts to dest
gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

// compile scss
gulp.task('sass', ['fonts'], function () {
    return gulp.src(scss.in)
        .pipe(autoprefixer({browsers: ['last 2 versions']}))
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sass(scss.sassOpts))
        .pipe(gulp.dest(scss.out))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('src/assets/css'))
});


gulp.task('browser-sync', function() {
    browserSync({

      server: {
          baseDir: "src" // Change this to your web root dir
      },
        files: ["src/assets/css/*.css", "src/assets/js/*.js", "src/*.html"],
    });
});

gulp.task('clean', function () {
  return del([
    'dist/*',
  ]);
});

gulp.task('copy-html', function () {
  gulp.src('src/*.html')
  .pipe(gulp.dest('dist'));
});
gulp.task('copy-fonts', function () {
  gulp.src('src/assets/fonts/*/*')
  .pipe(gulp.dest('dist/assets/fonts'))
});


gulp.task('img-min', function () {
  gulp.src('src/assets/images/*')
      .pipe(imagemin())
      .pipe(gulp.dest('dist/assets/images'))
});

gulp.task('gcmq', function () {
    gulp.src('src/assets/css/main.css')
        .pipe(gcmq())
        .pipe(gulp.dest('src/assets/css'));
});

gulp.task('purify', function() {
  var content = ['src/assets/js/*.js', 'src/*.html'];
  var css = ['src/assets/css/main.css'];

  var options = {
  minify: true,
  output: 'dist/pure.css'
  };

  purify(content, css, options, function (purifiedAndMinifiedResult) {
    console.log(purifiedAndMinifiedResult);
  });


});

gulp.task('uglifycss', function () {
  gulp.src('src/assets/css/main.css')
    .pipe(gcmq())
    .pipe(uglifycss({
  
    }))
    .pipe(gulp.dest('dist/assets/css'))
});

gulp.task('js', function () {
      return gulp.src([
                        'src/assets/js/jquery.min.js',
                        'src/assets/js/bootstrap.min.js',
                        'src/assets/js/bootstrap-datepicker.js',
                        'src/assets/js/jquery.jrumble.js',
                        'src/assets/js/app.js'
                      ])
      .pipe(sourcemaps.init())
      .pipe(jshint())
      .pipe(jshint.reporter('default'))
      .pipe(uglify())
      .pipe(concat('app.js'))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('dist/assets/js'))
});


// default task
gulp.task('default', ['sass', 'browser-sync'], function () {
     gulp.watch(scss.watch, ['sass']);
});

gulp.task('build', ['clean', 'img-min', 'copy-html', 'fonts', 'uglifycss', 'js']);
