$(document).ready(function() {


    if ($('.navbar-toggle').is(":hidden")) {

        $('ul.nav li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
        });
    }


    $('#nav').affix({
        offset: {
            top: $('#nav').offset().top
        }
    });

    $('.home-icon-nav').affix({
        offset: {
            top: $('#nav').offset().top
        }
    });


    if ($('.teaser-reservation').length) {

        var shakeHeight;
        var shakeHeight = $('#shake').offset().top;
        var correction = 0;
        var targetOffset = shakeHeight - correction;

        $('.teaser-reservation').affix({
            offset: {
                top: targetOffset
            }
        });
    }


    $('#shake').jrumble({
        x: 0,
        y: 0,
        rotation: 5,
        speed: 50,
    });


    $('#shake').hover(function() {
        $(this).trigger('startRumble');
        $(this).attr("src", "assets/images/teaser_reservation-rollover.png");
    }, function() {
        $(this).trigger('stopRumble');
        $(this).attr("src", "assets/images/teaser_reservation.png");

    });


    $('.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        language: 'de',
    });


    $('#monitor').html($(window).width());

    $(window).resize(function() {
        var viewportWidth = $(window).width();
        $('#monitor').html(viewportWidth);
    });



    $('ul.dropdown-menu li a').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 1000);
        }
    });



    // media query event handler
    if (matchMedia) {
        var mq = window.matchMedia("(min-width: 414px)");
        mq.addListener(WidthChange);
        WidthChange(mq);
    }

    // media query change
    function WidthChange(mq) {
        if (!mq.matches) {
            // window width is less than 414px
            //  var oldSrc = 'assets/images/logo.png';
            //  var newSrc = 'assets/images/logo-xs.png';
            //    $('img[src="' + oldSrc + '"]').attr('src', newSrc);
            $('img.logo').width('200px');
        }
    }


/*    var resizeTimer;

    $(window).on('resize', function(e) {

        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
            window.location.reload();

        }, 250);
    });
*/

if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  var resizeTimer;

      $(window).on('resize', function(e) {

          clearTimeout(resizeTimer);
          resizeTimer = setTimeout(function() {
              window.location.reload();

          }, 250);
      });
}


});
