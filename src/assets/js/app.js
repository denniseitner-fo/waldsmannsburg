$(document).ready(function() {


    if ($('.navbar-toggle').is(":hidden")) {

        $('ul.nav li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
        });
    }


    $('#nav').affix({
        offset: {
            top: $('#nav').offset().top
        }
    });

    $('.home-icon-nav').affix({
        offset: {
            top: $('#nav').offset().top
        }
    });


    if ($('.teaser-reservation').length) {

        var shakeHeight;
        var shakeHeight = $('#shake').offset().top;
        var correction = 0;
        var targetOffset = shakeHeight - correction;

        $('.teaser-reservation').affix({
            offset: {
                top: targetOffset
            }
        });
    }


    $('#shake').jrumble({
        x: 0,
        y: 0,
        rotation: 5,
        speed: 50,
    });


    $('#shake').hover(function() {
        $(this).trigger('startRumble');
        $(this).attr("src", "assets/images/teaser_reservation-rollover.png");
    }, function() {
        $(this).trigger('stopRumble');
        $(this).attr("src", "assets/images/teaser_reservation.png");

    });




    var dateToday = new Date();
    $(function() {
        $(".datepicker").datepicker({
            format: 'dd.mm.yyyy',
            startDate: '0',
            autoclose: true,
            language: 'de',
            locale: 'de',
            isRTL: false,

        });
    });


    $('#monitor').html($(window).width());

    $(window).resize(function() {
        var viewportWidth = $(window).width();
        $('#monitor').html(viewportWidth);
    });






    // media query event handler
    if (matchMedia) {
        var mq = window.matchMedia("(min-width: 414px)");
        mq.addListener(WidthChange);
        WidthChange(mq);
    }

    // media query change
    function WidthChange(mq) {
        if (!mq.matches) {
            // window width is less than 414px
            //  var oldSrc = 'assets/images/logo.png';
            //  var newSrc = 'assets/images/logo-xs.png';
            //    $('img[src="' + oldSrc + '"]').attr('src', newSrc);
            $('img.logo').width('200px');
        }
    }

    /*Form Validation*/
    $('#ContactForm').validate({

        focusInvalid: false,

        rules: {
            date: {
                minlength: 2,
                required: true,
                onfocusout: false

            },
            vorname: {
                minlength: 2,
                required: true
            },
            name: {
                minlength: 2,
                required: true
            },
            email: {
                required: true,
            },
            telefon: {
                required: true,
            },

            adresse: {
                required: true,
            },
            plz: {
                required: true,
            },
            ort: {
                required: true,
            },
            personen: {
                required: true,
            },
            stunde: {
                required: true,
            },
            minute: {
                required: true,
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-warning');
            $(element).closest('.form-inline').removeClass('has-success').addClass('has-warning');
        },
        errorPlacement: function(error, element) {}

    });



});
